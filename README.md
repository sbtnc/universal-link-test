# README #

## Why this repository? ##
The aim of this test repository, is to try iOS universal link.

## What does it contains? ##
This repository contains:

1. /apple-app-site-association (file to host on https domain validating app links)

2. /app-listening-ui (cordova project validating with https domain capabilities)

## What tutorial was followed? ##
Tutorial followed: http://blog.ionic.io/deeplinking-in-ionic-apps/

cordova plugin add ionic-plugin-deeplinks --variable URL_SCHEME=<schema> --variable DEEPLINK_SCHEME=https --variable DEEPLINK_HOST=<host>
<schema> for test: sebc
<host> for test: cmscn-int.bmwgroup.com

## What should you test? ##
Acceptance criteria:
From a link https://cmscn-int.bmwgroup.com/test/* should open already INSTALLED com.mcon.group.universal.links.test2 app.

## Some other details... ##
On developer.apple.com with our Enterprise license:
- creation of an app_ID with "associated domains" -> DONE
- creation of dist provisioning profile for team 898L3QZMUQ -> DONE

## How to setup the test app? ##
![1.png](https://bitbucket.org/repo/RbrzAo/images/1581713169-1.png)
![2.png](https://bitbucket.org/repo/RbrzAo/images/1300374362-2.png)
![3.png](https://bitbucket.org/repo/RbrzAo/images/2260586821-3.png)
![4.png](https://bitbucket.org/repo/RbrzAo/images/560192266-4.png)
![5.png](https://bitbucket.org/repo/RbrzAo/images/3266326356-5.png)

